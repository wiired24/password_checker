########################################################################
#Simple Password Checker                                                
#(C) Zach McPherson 2020
#
#As long as you retain this notice you can do whatever you want with 
#this stuff. If we meet some day, and you think this stuff is worth 
#it, you can buy me a beer in return - Licensed Under Beerware
########################################################################
require 'colorize'


def password_check()           
	play_again = true
	while play_again == true do
	
	puts "Enter the Password you would like to test:"
	puts "----------------------------------------- "
	password = gets
	
	if password.match?(/\w.{12,45}/)
		puts "Your password doesn't suck! 🤗".colorize(:green)
	else
		puts "You need a better password! 😥".colorize(:red)
		end
	
	puts "Would you like to test another password? (Y/N)"
	response = gets
	response = response.downcase
	if response == "n\n"
		play_again = false
		break
		
		end		   
	end
end

password_check()
