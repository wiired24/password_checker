This is just a simple password checker written in Ruby. I made this as a fun
project to flex my programming muscles with regular expressions. 

The idea is that you supply a password, ruby then checks it against the regex
and it will tell you if your password sucks or not. 

***Instructions for installing Password_Checker are as follows***


1. git clone the project to your local machine
2. run gem install bundler (if not installed)
3. open up the folder you just downloaded
4. run bundle install to install dependencies
5. now execute password_checker.rb
